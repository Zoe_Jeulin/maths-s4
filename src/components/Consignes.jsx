import '../css/style.css';
import React from 'react';
import { Link } from 'react-router-dom';

const Consignes = () => {

    return(
        <div id="div_background">
            <span id="content">
                <h1>REGLES DU JEU</h1>
                <p>Vous allez jouer au Blind'Alea Test !</p>
                <p>Dans un premier temps, vous allez devoir choisir la catégorie dans laquelle vous souhaitez concourir.</p>
                <p>Vous avez le choix entre trois catégories : musique, film et série. Vous pouvez également choisir de jouer en aléatoire.</p>
                <p>Dans ce cas, vous avez une chance sur 7 de tomber sur une catégorie ou sur une combinaison de catégories.</p>
                <p>Une fois ce choix fait, vous pourrez passer à l'écran suivant afin de choisir la difficulté du jeu.</p>
                <p>Une fois l'extrait lancé, vous n'avez plus qu'à deviner l'oeuvre. Si vous obtenez un nombre trop grand d'erreurs, la première </p>
                <p>lettre vous sera donnée. Enfin, très rarement, il est possible qu'un joker apparaisse et vous permette de connaître la réponse pour un extrait !</p>
                <Link to="/categories">
                    <button type="button" className="margin-top">
                        C'est parti !
                    </button>
                </Link>
            </span>
        </div>
    );
}

export default Consignes;
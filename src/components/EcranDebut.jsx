import '../css/style.css';
import React from 'react';
import { Link } from 'react-router-dom';

const EcranDebut = () => {
    return(
        <div className="div_background">
            <span className="content">
                <h1>BIENVENUE DANS LE BLIND'ALEA TEST !</h1>
                <Link to="/consignes">
                    <button type="button">
                        Nouvelle partie
                    </button>
                </Link>
            </span>
        </div>
    );
}

export default EcranDebut;
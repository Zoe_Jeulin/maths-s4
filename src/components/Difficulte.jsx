import '../css/style.css';
import React from 'react';
import { Link } from 'react-router-dom';

const Difficulte = (props) => {
    return(
        <div id="div_background">
            <span id="content">
                <h1>CHOIX DE LA DIFFICULTÉ</h1>

                <h2>Durée de l'extrait</h2>
                <input type="radio" id="ext_court" name="radio_duree" value="ext_court" defaultChecked onChange={props.onClickCourt} />
                <label htmlFor="ext_court">Plutôt court</label>
                <input type="radio" id="ext_long" name="radio_duree" value="ext_long" onChange={props.onClickLong} />
                <label htmlFor="ext_long">Plutôt long</label>

                <h2>Fréquence des indices</h2>
                <input type="radio" id="indice_min" name="radio_indice" value="indice_min" defaultChecked onChange={props.onClickIndiceMin} />
                <label htmlFor="indice_min">Un tout petit peu</label>
                <input type="radio" id="indice_max" name="radio_indice" value="indice_max" onChange={props.onClickIndiceMax} />
                <label htmlFor="indice_max">Un petit peu</label>

                <h2>Fréquence des jokers</h2>
                <input type="radio" id="joker_min" name="radio_joker" value="joker_min" defaultChecked onChange={props.onClickJokerMin} />
                <label htmlFor="joker_min">Très très peu</label>
                <input type="radio" id="joker_max" name="radio_joker" value="joker_max" onChange={props.onClickJokerMax} />
                <label htmlFor="joker_max">Très peu</label>
                
                <div>
                    <Link to="/extrait">
                        <button type="button" className="margin-top" onClick={props.onSubmit}>
                            Valider
                        </button>
                    </Link>
                </div>    
            </span>
        </div>
    );
}

export default Difficulte;
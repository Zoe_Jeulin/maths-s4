import '../css/style.css';
import React from 'react';
import { Link } from 'react-router-dom';

const Categories = (props) => {
    return(
        <div id="div_background">
            <span id="content">
                <h1>CATEGORIES</h1>
                <input type="radio" id="cat_manuel" name="radio_choix" value="cat_manuel" defaultChecked onChange={props.onClickManuel} />
                <label htmlFor="cat_manuel">Choisir manuellement</label>
                <input type="radio" id="cat_alea" name="radio_choix" value="cat_alea" onChange={props.onClickAleatoire} />
                <label htmlFor="cat_alea">Jouer en aléatoire</label>
                
                {props.choixManuel === 1 ? 
                    <div>
                        <input type="checkbox" id="cat_musiques" name="checkbox_categories" value="cat_musiques" onChange={props.onChangeCheckbox} />
                        <label htmlFor="cat_musiques">Musiques</label>
        
                        <input type="checkbox" id="cat_films" name="checkbox_categories" value="cat_films" onChange={props.onChangeCheckbox} />
                        <label htmlFor="cat_films">Films</label>
        
                        <input type="checkbox" id="cat_series" className="margin-bottom" name="checkbox_categories" value="cat_series" onChange={props.onChangeCheckbox} />
                        <label htmlFor="cat_series">Séries</label>
                        {props.categories.length === 0 ?
                            <h3>Veuillez choisir au moins une catégorie</h3>
                            :
                            <Link to="/difficulte">
                                <button type="button" className="margin-top">
                                    Valider
                                </button>
                            </Link>
                        }
                    </div>
                    :
                    <div>
                        <Link to="/difficulte">
                            <button type="button" className="margin-top">
                                Valider
                            </button>
                        </Link>
                    </div>                  
                }
            </span>
        </div>
    );
}

export default Categories;
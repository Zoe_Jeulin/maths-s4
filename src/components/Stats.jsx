import '../css/style.css';
import { Link } from 'react-router-dom';

const Stats = (props) => {
    return(
        <div id="div_background">
            <span id="content">
                <h1>STATS</h1> 
                <h3>{`Nombre d'extraits trouvés : ${props.repOk}/${props.nbRound} - Nombre d'essais total : ${props.cptEssai}`}</h3>
                <h2>Moment de l'extrait (Loi de Bernoulli)</h2>
                <p>{`Début : ${(props.cptDebut/props.nbRound*100).toString().substring(0,4)}% (théorique : 25%)`}</p>
                <p>{`Fin : ${(props.cptFin/props.nbRound*100).toString().substring(0,4)}% (théorique : 75%)`}</p> 

                <h2>Durée supplémentaire (Loi binomiale)</h2>
                <p>{`+0sec : ${(props.cpt0s/props.nbRound*100).toString().substring(0,4)}% (théorique : 0.41% - 26.21%)`}</p>
                <p>{`+1sec : ${(props.cpt1s/props.nbRound*100).toString().substring(0,4)}% (théorique : 3.69% - 39.32%)`}</p>
                <p>{`+2sec : ${(props.cpt2s/props.nbRound*100).toString().substring(0,4)}% (théorique : 13.82% - 24.58%)`}</p>
                <p>{`+3sec : ${(props.cpt3s/props.nbRound*100).toString().substring(0,4)}% (théorique : 8.19% - 27.65%)`}</p>
                <p>{`+4sec : ${(props.cpt4s/props.nbRound*100).toString().substring(0,4)}% (théorique : 1.54% - 31.10%)`}</p>
                <p>{`+5sec : ${(props.cpt5s/props.nbRound*100).toString().substring(0,4)}% (théorique : 0.15% - 18.66%)`}</p>
                <p>{`+6sec : ${(props.cpt6s/props.nbRound*100).toString().substring(0,4)}% (théorique : 0.01% - 4.67%)`}</p>

                <h2>Fréquence des indices (Loi géométrique)</h2>
                <p>{props.cptEssai === 0 ? `0% (théorique : %)` : `${(props.cptIndice/props.cptEssai*100).toString().substring(0,4)}% (théorique : <10% - <20%)`}</p>

                <h2>Fréquence des jokers (Loi de Poisson)</h2>
                <p>{props.cptEssai === 0 ? `0% (théorique : %)` : `${(props.cptJoker/props.cptEssai*100).toString().substring(0,4)}% (théorique : 1.49% - 7.33%)`}</p>

                <Link to="/extrait">
                    <button type="button">
                        Retour au jeu
                    </button>
                </Link>
            </span>
        </div>
    );
}

export default Stats;
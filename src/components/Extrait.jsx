import '../css/style.css';
import React from 'react';
import { Link } from 'react-router-dom';

const Extrait = (props) => {
    console.log(props.extrait);

    const fin = props.debut+4+props.duree;
    
    if(props.extrait !== undefined && props.extrait !== null){
        return(
            <div id="div_background">
                <span id="content">
                    <h1>{`EXTRAIT N°${props.round}`}</h1>
                    <div id="div_video">
                        <iframe width="560" height="315" src={`${props.extrait.lien}?autoplay=1&controls=0&start=${props.debut}&end=${fin}`} title="YouTube video player" frameBorder="0" allowFullScreen></iframe>
                    </div>
                    <h2>D'où provient cet extrait ?</h2>
                    <input type="text" onChange={props.onChangeAnswer.bind(this)}></input>
                    {props.indice === 1 ?
                        <p>{`La 1ère lettre est ${props.extrait.titre[0]}`}</p>
                        :
                        <p></p>
                    }

                    {props.joker === 1 ?
                        <p>{`JOKER ! La réponse est ${props.extrait.titre}`}</p>
                        :
                        <p></p>
                    }  

                    {props.essai > 0 ?
                        <p>Mauvaise réponse !</p>
                        :
                        <p></p>
                    }         
                

                    <Link to="/extrait">
                        <button type="button" onClick={props.onClickAnswer}>
                            Valider
                        </button>
                    </Link>                

                    <Link to="/stats">
                        <button type="button">
                            Voir les stats
                        </button>
                    </Link>

                    <Link to="/extrait">
                        <button type="button" onClick={props.onClickPasser}>
                            Passer
                        </button>
                    </Link>
                </span>
            </div>
        );
    }else{
        return null;
    }
}

export default Extrait;
import React, { useState } from 'react';
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import EcranDebut from './components/EcranDebut.jsx';
import Consignes from './components/Consignes.jsx';
import Categories from './components/Categories.jsx';
import Difficulte from './components/Difficulte.jsx';
import Extrait from './components/Extrait.jsx';
import Stats from './components/Stats.jsx';

import films from './data/films.json';
import musiques from './data/musiques.json';
import series from './data/series.json';

const Chemin = () => {
    const [manuel, setManuel] = useState(1);
    const [listCat, setListCat] = useState([]);
    const [listeExtraits, setListeExtraits] = useState([]);
    const [extrait, setExtrait] = useState(null);
    const [numRound, setRound] = useState(0);
    const [answer, setAnswer] = useState(0);
    const [nbEssai, setNbEssai] = useState(0);
    const [duree, setDuree] = useState(0);
    const [debut, setDebut] = useState(0);
    const [nDuree, setNDuree] = useState(3);
    const [pDuree, setPDuree] = useState(0.3);
    const [probaIndice, setProbaIndice] = useState(0.1);
    const [probaJoker, setProbaJoker] = useState(5);
    const [indice, setIndice] = useState(0);
    const [joker, setJoker] = useState(0);
    const [cptIndice, setCptIndice] = useState(0);
    const [cptJoker, setCptJoker] = useState(0);
    const [cptDebut, setCptDebut] = useState(0);
    const [cptFin, setCptFin] = useState(0);
    const [cptEssai, setCptEssai] = useState(0);
    const [cpt0s, setCpt0s] = useState(0);
    const [cpt1s, setCpt1s] = useState(0);
    const [cpt2s, setCpt2s] = useState(0);
    const [cpt3s, setCpt3s] = useState(0);
    const [cpt4s, setCpt4s] = useState(0);
    const [cpt5s, setCpt5s] = useState(0);
    const [cpt6s, setCpt6s] = useState(0);
    const [cptRepOk, setCptRepOk] = useState(0);

    //CATEGORIES.JSX
    const handleCheckbox = (e) => {
        setListeExtraits(listeExtraits => []);
        var newList = [...listCat, e.target.id];
        if(listCat.includes(e.target.id)){
            newList = newList.filter(elem => elem !== e.target.id); 
        }
        setListCat(newList);

        if(newList.includes('cat_musiques')){
            setListeExtraits(listeExtraits => [...listeExtraits, ...musiques]);
        }
        if(newList.includes('cat_films')){
            setListeExtraits(listeExtraits => [...listeExtraits, ...films]);
        }
        if(newList.includes('cat_series')){
            setListeExtraits(listeExtraits => [...listeExtraits, ...series]);
        }
    }

    const handleChoixAleatoire = () => {
        setManuel(0);
        setListCat(listCat => []);
        setListeExtraits(listeExtraits => []);
        //LOI UNIFORME
        const random = Math.floor(Math.random() * 7);
        switch(random){
            case 0:
                setListeExtraits(listeExtraits => [...listeExtraits, ...musiques]);
                break;
            case 1:
                setListeExtraits(listeExtraits => [...listeExtraits, ...films]);
                break;
            case 2:
                setListeExtraits(listeExtraits => [...listeExtraits, ...series]);
                break;
            case 3:
                setListeExtraits(listeExtraits => [...listeExtraits, ...musiques, ...films]);
                break;
            case 4:
                setListeExtraits(listeExtraits => [...listeExtraits, ...musiques, ...series]);
                break;
            case 5:
                setListeExtraits(listeExtraits => [...listeExtraits, ...films, ...series]);
                break;
            case 6:
                setListeExtraits(listeExtraits => [...listeExtraits, ...musiques, ...films, ...series]);
                break;
            default:
                break;
        }
    }

    const handleList = () => {
        setManuel(1);
        setListCat([]);
    }

    const fact = (n) => {
        var factK = 1;
        for(var i = 1; i<=n; i++){
            factK *= i;
        }
        return factK;
    }

    //DIFFICULTE.JSX
    const dureeCourt = () => {
        setNDuree(6);
        setPDuree(0.2);
    }

    const dureeLong = () => {
        setNDuree(6);
        setPDuree(0.6);
    }

    const handleDuree = () => {
        //LOI BINOMIALE
        const random = Math.random();
        const p0 = (fact(nDuree)/(fact(0)*fact(nDuree-0)))*Math.pow(pDuree,0)*Math.pow(1-pDuree, nDuree-0);
        const p1 = (fact(nDuree)/(fact(1)*fact(nDuree-1)))*Math.pow(pDuree,1)*Math.pow(1-pDuree, nDuree-1);
        const p2 = (fact(nDuree)/(fact(2)*fact(nDuree-2)))*Math.pow(pDuree,2)*Math.pow(1-pDuree, nDuree-2);
        const p3 = (fact(nDuree)/(fact(3)*fact(nDuree-3)))*Math.pow(pDuree,3)*Math.pow(1-pDuree, nDuree-3);
        const p4 = (fact(nDuree)/(fact(4)*fact(nDuree-4)))*Math.pow(pDuree,4)*Math.pow(1-pDuree, nDuree-4);
        const p5 = (fact(nDuree)/(fact(5)*fact(nDuree-5)))*Math.pow(pDuree,5)*Math.pow(1-pDuree, nDuree-5);

        if(random < p0){
            setDuree(0);
            setCpt0s(cpt0s => cpt0s+1);
        }else if(random < p0+p1){
            setDuree(1);
            setCpt1s(cpt1s => cpt1s+1);
        }else if(random < p0+p1+p2){
            setDuree(2);
            setCpt2s(cpt2s => cpt2s+1);
        }else if(random < p0+p1+p2+p3){
            setDuree(3);
            setCpt3s(cpt3s => cpt3s+1);
        }else if(random < p0+p1+p2+p3+p4){
            setDuree(4);
            setCpt4s(cpt4s => cpt4s+1);
        }else if(random < p0+p1+p2+p3+p4+p5){
            setDuree(5);
            setCpt5s(cpt5s => cpt5s+1);
        }else{
            setDuree(6);
            setCpt6s(cpt6s => cpt6s+1);
        }
    }

    const indiceMin = () => {
        setProbaIndice(0.1); //plus dur (<10%)
    }

    const indiceMax = () => {
        setProbaIndice(0.2); //plus facile (<20%)
    }

    const jokerMin = () => {
        if(probaIndice === 0.1){
            setProbaJoker(5); //plus facile (3.37%)
        }else{
            setProbaJoker(6); //plus dur (1.49%)
        }
    }

    const jokerMax = () => {
        if(probaIndice === 0.1){
            setProbaJoker(4); //plus facile (7.33%)
        }else{
            setProbaJoker(5); //plus dur (3.37%)
        }
    }

    //EXTRAIT.JSX
    const handleAnswer = (e) => {
        setAnswer(e.target.value);
    }

    const checkAnswer = () => {
        if(answer.toString().toLowerCase() === extrait.titre.toLowerCase()){
            setCptRepOk(cptRepOk => cptRepOk+1);
            nextRound();
        }else{
            setNbEssai(nbEssai => nbEssai+1);
            setCptEssai(cptEssai => cptEssai+1);

            //LOI GEOMETRIQUE
            const pKGeometrique = probaIndice*Math.pow(1-probaIndice, nbEssai-1);
            const randomGeometrique = Math.random();
            if(randomGeometrique < pKGeometrique){
                setIndice(1);
                setCptIndice(cptIndice => cptIndice+1);
            }

            //LOI DE POISSON
            const pKPoisson = Math.exp((-probaJoker))*(Math.pow(probaJoker,1)/1);
            const randomPoisson = Math.random();
            if(randomPoisson < pKPoisson){
                setJoker(1);
                setCptJoker(cptJoker => cptJoker+1);
            }
        }
    }

    const nextRound = () => {
        setRound(numRound => numRound+1);
        setNbEssai(0);
        setIndice(0);
        setJoker(0);
        handleDuree();
        setAnswer("");
        setExtrait(listeExtraits[Math.floor(Math.random()*listeExtraits.length)]);

        //LOI BERNOULLI
        const random2 = Math.random();
        if(random2 <= 0.75) {
            setDebut(60);
            setCptFin(cptFin => cptFin+1);
        }else{
            setDebut(10);
            setCptDebut(cptDebut => cptDebut+1);
        }
    }

    return(
        <Router>
            <Switch>
                <Route path="/home">
                    <EcranDebut />
                </Route>
                <Route path="/consignes">
                    <Consignes />
                </Route>
                <Route path="/categories">
                    <Categories onChangeCheckbox={handleCheckbox} onClickAleatoire={handleChoixAleatoire} onClickManuel={handleList} choixManuel={manuel} categories={listCat} />
                </Route>
                <Route path="/difficulte">
                    <Difficulte onClickCourt={dureeCourt} onClickLong={dureeLong} onClickIndiceMin={indiceMin} onClickIndiceMax={indiceMax} onClickJokerMin={jokerMin} onClickJokerMax={jokerMax} onSubmit={nextRound} />
                </Route>
                <Route path="/extrait">
                    <Extrait extrait={extrait} debut={debut} duree={duree} indice={indice} joker={joker} onClickRound={nextRound} onChangeAnswer={handleAnswer} onClickAnswer={checkAnswer} onClickPasser={nextRound} round={numRound} essai={nbEssai} />
                </Route>
                <Route path="/stats">
                    <Stats nbRound={numRound} repOk={cptRepOk} cptDebut={cptDebut} cptFin={cptFin} cptIndice={cptIndice} cptJoker={cptJoker} cpt0s={cpt0s} cpt1s={cpt1s} cpt2s={cpt2s} cpt3s={cpt3s} cpt4s={cpt4s} cpt5s={cpt5s} cpt6s={cpt6s} cptEssai={cptEssai} />
                </Route>
            </Switch>
        </Router>
    )
};

export default Chemin;
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Chemin from './Chemin';

ReactDOM.render(
  <React.StrictMode>
    {/* <App /> */}
    <Chemin />
  </React.StrictMode>,
  document.getElementById('root')
);

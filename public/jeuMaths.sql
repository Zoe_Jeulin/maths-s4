-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: May 21, 2021 at 02:20 PM
-- Server version: 5.7.32
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jeuMaths`
--

-- --------------------------------------------------------

--
-- Table structure for table `maths-s4-films`
--

CREATE TABLE `maths-s4-films` (
  `ID` int(3) NOT NULL,
  `Titre` varchar(50) NOT NULL,
  `Réalisateur` varchar(25) NOT NULL,
  `Lien` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `maths-s4-films`
--

INSERT INTO `maths-s4-films` (`ID`, `Titre`, `Réalisateur`, `Lien`) VALUES
(1, 'Shutter Island', 'Martin Scorsese', 'https://www.youtube.com/watch?v=5iaYLCiq5RM'),
(2, 'Dirty Dancing ', 'Emile Ardolino', 'https://www.youtube.com/watch?v=eIcmQNy9FsM'),
(3, 'Forrest Gump', 'Robert Zemeckis', 'https://www.youtube.com/watch?v=bLvqoHBptjg'),
(4, 'Catch Me If You Can ', 'Steven Spielberg', 'https://www.youtube.com/watch?v=71rDQ7z4eFg'),
(5, 'The Imitation Game', 'Morten Tyldum', 'https://www.youtube.com/watch?v=nuPZUUED5uk'),
(6, 'Titanic', 'Steven Spielberg', 'https://www.youtube.com/watch?v=ZQ6klONCq4s'),
(7, 'Le Fabuleux Destin d\'Amélie Poulain', 'Jean-Pierre Jeunet', 'https://www.youtube.com/watch?v=FBh0Bt5fmEo'),
(8, 'Fight Club', 'David Fincher', 'https://www.youtube.com/watch?v=qtRKdVHc-cE'),
(9, 'Pulp Fiction', 'Quentin Tarentino', 'https://www.youtube.com/watch?v=s7EdQ4FqbhY'),
(10, 'Interstellar', 'Christopher Nolan', 'https://www.youtube.com/watch?v=zSWdZVtXT7E'),
(11, 'Inception', 'Christopher Nolan', 'https://www.youtube.com/watch?v=YoHD9XEInc0'),
(12, '2001 : L\'Odyssée De l\'Espace', 'Stanley Kubrick', 'https://www.youtube.com/watch?v=oR_e9y-bka0'),
(13, 'Le Parrain', 'Francis Ford Copola', 'https://www.youtube.com/watch?v=sY1S34973zA'),
(14, 'Le Seigneur Des Anneaux', 'Peter Jackson', 'https://www.youtube.com/watch?v=V75dMMIW2B4'),
(15, 'Mulholland Drive', 'David Lynch', 'https://www.youtube.com/watch?v=jbZJ487oJlY'),
(16, 'Matrix', 'Wachowski', 'https://www.youtube.com/watch?v=m8e-FF8MsqU'),
(17, 'Retour Vers Le Futur', 'Robert Zemeckis', 'https://www.youtube.com/watch?v=qvsgGtivCgs'),
(18, '12 Hommes En Colère', 'Sydney Lumet', 'https://www.youtube.com/watch?v=_13J_9B5jEk'),
(19, 'Into the Wild', 'Sean Penn', 'https://www.youtube.com/watch?v=XZG1FzyB8DI'),
(20, 'Lucy', 'Luc Besson', 'https://www.youtube.com/watch?v=bN7ksFEVO9U'),
(21, 'La ligne verte', 'Frank Darabont', 'https://www.youtube.com/watch?v=Ki4haFrqSrw'),
(22, 'The Truman Show', 'Peter Weir', 'https://www.youtube.com/watch?v=dlnmQbPGuls'),
(23, 'Mommy', 'Xavier Dolan', 'https://www.youtube.com/watch?v=PQRg5X7B9is'),
(24, 'Jurassic Park', 'Steven Spielberg', 'https://www.youtube.com/watch?v=QWBKEmWWL38'),
(25, 'La Liste De Schindler', 'Steven Spielberg', 'https://www.youtube.com/watch?v=gG22XNhtnoY'),
(26, 'La La Land', 'Damien Chazelle', 'https://www.youtube.com/watch?v=0pdqf4P9MB8'),
(27, 'Le Pianiste', 'Roman Polanski', 'https://www.youtube.com/watch?v=BFwGqLa_oAo'),
(28, 'Bienvenue A Gattaca', 'Andrew Niccol', 'https://www.youtube.com/watch?v=W_KruQhfvW4'),
(29, 'Le Prestige', 'Christopher Nolan', 'https://www.youtube.com/watch?v=RLtaA9fFNXU'),
(30, 'Memento', 'Christopher Nolan', 'https://www.youtube.com/watch?v=HDWylEQSwFo'),
(31, 'Whiplash', 'Damien Chazelle', 'https://www.youtube.com/watch?v=7d_jQycdQGo'),
(32, 'The Grand Budapest Hotel', 'Wes Anderson', 'https://www.youtube.com/watch?v=1Fg5iWmQjwk'),
(33, 'Astérix Mission Cléopâtre', 'Alain Chabat', 'https://www.youtube.com/watch?v=DR1a7R5usts'),
(34, 'Rrrrr', 'Alain Chabat', 'https://www.youtube.com/watch?v=y_UKPVezalU'),
(35, 'La Cité De La Peur', 'Alain Berbérian', 'https://www.youtube.com/watch?v=8Drj9uNHgfs'),
(36, 'La Haine', 'Mathieu Kassovitz', 'https://www.youtube.com/watch?v=G65Y-yr4M4o'),
(37, 'Rocky', 'John G. Alvidsen', 'https://www.youtube.com/watch?v=7RYpJAUMo2M'),
(38, 'Star Wars', 'Georges Lucas', 'https://www.youtube.com/watch?v=bD7bpG-zDJQ'),
(39, 'Ocean\'s Eleven', 'Steven Soderbergh', 'https://www.youtube.com/watch?v=imm6OR605UI'),
(40, 'Shining', 'Stanley Kubrick', 'https://www.youtube.com/watch?v=i-B_bbkEfS0'),
(41, 'Parasite', 'Bong Joon-ho', 'https://www.youtube.com/watch?v=5xH0HfJHsaY'),
(42, 'Black Panther', 'Ryan Coogler', 'https://www.youtube.com/watch?v=xjDjIWPwcPU'),
(44, 'The Holiday ', 'Nancy Meyers', 'https://www.youtube.com/watch?v=BDi5zH18vxU'),
(45, 'Django Unchained', 'Quentin Tarentino', 'https://www.youtube.com/watch?v=0fUCuvNlOCg'),
(46, 'Mamma Mia', 'Phyllida Lloyd', 'https://www.youtube.com/watch?v=8RBNHdG35WY'),
(47, 'Split', 'M. Night Shyamalan', 'https://www.youtube.com/watch?v=84TouqfIsiI'),
(48, 'Skyfall', 'Sam Mendes', 'https://www.youtube.com/watch?v=6kw1UVovByw'),
(49, 'Love Actually', 'Richard Curtis', 'https://www.youtube.com/watch?v=H9Z3_ifFheQ'),
(50, 'Avatar', 'James Cameron', 'https://www.youtube.com/watch?v=5PSNL1qE6VY');

-- --------------------------------------------------------

--
-- Table structure for table `maths-s4-musiques`
--

CREATE TABLE `maths-s4-musiques` (
  `ID` int(3) NOT NULL,
  `Titre` varchar(50) NOT NULL,
  `Auteur` varchar(50) NOT NULL,
  `Année` int(4) NOT NULL,
  `Lien` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `maths-s4-musiques`
--

INSERT INTO `maths-s4-musiques` (`ID`, `Titre`, `Auteur`, `Année`, `Lien`) VALUES
(1, 'Bohemian Rhapsody', 'Queen', 1975, 'https://www.youtube.com/watch?v=fJ9rUzIMcZQ'),
(2, 'Highway To Hell', 'ACDC', 1979, 'https://www.youtube.com/watch?v=l482T0yNkeo'),
(3, 'Stairway To Heaven', 'Led Zeppelin', 1971, 'https://www.youtube.com/watch?v=IS6n2Hx9Ykk'),
(4, 'Break My Heart', 'Dua Lipa', 2020, 'https://www.youtube.com/watch?v=Nj2U6rhnucI'),
(5, '7 rings', 'Ariana Grande', 2019, 'https://www.youtube.com/watch?v=QYh6mYIJG2Y'),
(6, 'Blinding Lights', 'The weeknd', 2019, 'https://www.youtube.com/watch?v=fHI8X4OXluQ'),
(7, 'You\'re The One That I Want ', 'Olivia Newton-John, John Travolta ', 1978, 'https://www.youtube.com/watch?v=itRFjzQICJU'),
(8, 'Take On Me', 'a-ha', 1985, 'https://www.youtube.com/watch?v=djV11Xbc914'),
(9, 'The Time Of My Life ', 'Bill Medley, Jennifer Warnes', 1987, 'https://www.youtube.com/watch?v=4BQLE_RrTSU'),
(10, 'Money, Money, Money', 'Abba', 1976, 'https://www.youtube.com/watch?v=ETxmCCsMoD0'),
(11, 'Thriller', 'Michael Jackson', 1982, 'https://www.youtube.com/watch?v=4V90AmXnguw'),
(12, 'Tombé', 'Matt Pokora', 2019, 'https://www.youtube.com/watch?v=HX5l5DxmmBw'),
(13, 'Baby', 'Justin Bieber', 2010, 'https://www.youtube.com/watch?v=kffacxfA7G4'),
(14, 'Roar', 'Katy Perry', 2013, 'https://www.youtube.com/watch?v=CevxZvSJLk8'),
(15, 'Crazy In Love', 'Beyoncé', 2003, 'https://www.youtube.com/watch?v=ViwtNLUqkMY'),
(16, 'Umbrella', 'Rihanna', 2008, 'https://www.youtube.com/watch?v=CvBfHwUxHIk'),
(17, 'Come Together', 'Les Beatles', 1969, 'https://www.youtube.com/watch?v=45cYwDMibGo'),
(18, 'Can\'t Stop', 'Red Hot', 2002, 'https://www.youtube.com/watch?v=8DyziWtkfBw'),
(19, 'Paint It, Black', 'Les Rollings Stones', 1966, 'https://www.youtube.com/watch?v=O4irXQhgMqg'),
(20, 'Wish You Were Here', 'Pink Floyd', 1975, 'https://www.youtube.com/watch?v=IXdNnw99-Ic'),
(21, 'Sunday Bloody Sunday', 'U2', 1983, 'https://www.youtube.com/watch?v=CkeDjLZMRRk'),
(22, 'Thunderstruck', 'AC/DC', 1990, 'https://www.youtube.com/watch?v=v2AC41dglnM'),
(23, 'Wonderwall', 'Oasis', 1995, 'https://www.youtube.com/watch?v=bx1Bh8ZvH84'),
(24, '3 Nuits Par Semaine', 'Indochine', 1985, 'https://www.youtube.com/watch?v=Mtpnv1D-HXo'),
(25, 'Personal Jesus', 'Depeche Mode', 1989, 'https://www.youtube.com/watch?v=u1xrNaTO1bI'),
(26, 'Uprising', 'Muse', 2009, 'https://www.youtube.com/watch?v=w8KQmps-Sog'),
(27, 'Message In a Bottle', 'The Police', 1979, 'https://www.youtube.com/watch?v=MbXWrmQW-OE'),
(28, 'Cendrillon', 'Téléphone', 1982, 'https://www.youtube.com/watch?v=_tHbSMQeCdk'),
(29, 'Hotel California', 'The Eagles', 1976, 'https://www.youtube.com/watch?v=EqPtz5qN7HM'),
(30, 'Paradise', 'Coldplay', 2011, 'https://www.youtube.com/watch?v=1G4isv_Fylg'),
(31, 'Radioactive', 'Imagine Dragons', 2012, 'https://www.youtube.com/watch?v=ktvTqknDobU'),
(32, 'Allumez Le Feu', 'Johnny Hallyday', 1998, 'https://www.youtube.com/watch?v=s3O1Xro7oAI'),
(33, 'Pour Que Tu M\'aimes Encore', 'Céline Dion', 1995, 'https://www.youtube.com/watch?v=AzaTyxMduH4'),
(34, 'Can\'t Help Falling In Love', 'Elvis Presley', 1961, 'https://www.youtube.com/watch?v=vGJTaP6anOU'),
(35, 'Poker Face', 'Lady Gaga', 2008, 'https://www.youtube.com/watch?v=bESGLojNYSo'),
(36, 'Holiday', 'Madonna', 1983, 'https://www.youtube.com/watch?v=IHknoSQZibU'),
(37, 'I\'m Still Standing', 'Elton John', 1983, 'https://www.youtube.com/watch?v=ZHwVBirqD2s'),
(38, 'Let\'s Dance', 'David Bowie', 1983, 'https://www.youtube.com/watch?v=VbD_kBJc_gI'),
(39, 'Cry Me A River', 'Justin Timberlake', 2002, 'https://www.youtube.com/watch?v=DksSPZTZES0'),
(40, 'Hotline Bling', 'Drake', 2015, 'https://www.youtube.com/watch?v=uxpDa-c-4Mc'),
(41, 'All I Want For Christmas Is You', 'Mariah Carey', 1994, 'https://www.youtube.com/watch?v=yXQViqx6GMY'),
(42, 'Toxic', 'Britney Spears', 2003, 'https://www.youtube.com/watch?v=LOZuxwVk7TU'),
(43, 'Shape Of You', 'Ed Sheeran', 2017, 'https://www.youtube.com/watch?v=JGwWNGJdvx8'),
(44, 'Kiss', 'Prince', 1986, 'https://www.youtube.com/watch?v=H9tEvfIsDyo'),
(45, 'La Bohème', 'Charles Aznavour', 1965, 'https://www.youtube.com/watch?v=fVfnEyLOkrM'),
(46, 'Ne Me Quitte Pas', 'Jacques Brel', 1959, 'https://www.youtube.com/watch?v=i2wmKcBm4Ik'),
(47, 'La Javanaise', 'Serge Gainsbourg', 1963, 'https://www.youtube.com/watch?v=V6gjzNm6dA0'),
(48, 'Alexandrie Alexandra', 'Claude François', 1977, 'https://www.youtube.com/watch?v=HkVhN64dyd8'),
(49, 'Mistral Gagnant', 'Renaud', 1985, 'https://www.youtube.com/watch?v=_YqzuE-5RE8'),
(50, 'No Woman, No Cry', 'Bob Marley', 1974, 'https://www.youtube.com/watch?v=pHlSE9j5FGY'),
(51, 'Quand La Musique Est Bonne', 'Jean-Jacques Goldman', 1982, 'https://www.youtube.com/watch?v=-boDeijWuOY'),
(52, 'Shake It Off', 'Taylor Swift', 2014, 'https://www.youtube.com/watch?v=nfWlot6h_JM'),
(53, 'La Vie En Rose', 'Edith Piaf', 1947, 'https://www.youtube.com/watch?v=kFzViYkZAz4'),
(54, 'I Wanna Dance With Somebody', 'Whitney Houston', 1987, 'https://www.youtube.com/watch?v=eH3giaIzONA'),
(55, 'Bande Organisée', 'Jul', 2020, 'https://www.youtube.com/watch?v=-CVn3-3g_BI'),
(56, 'Mona Lisa', 'Booba', 2021, 'https://www.youtube.com/watch?v=Cp9pk-FkE6E'),
(57, 'Elle pleut', 'Nekfeu', 2019, 'https://www.youtube.com/watch?v=cBKGKkQnI94'),
(58, 'Basique', 'Orelsan', 2017, 'https://www.youtube.com/watch?v=2bjk26RwjyU'),
(59, 'Macarena', 'Damso', 2017, 'https://www.youtube.com/watch?v=GGhKPm18E48'),
(60, 'Empire State Of Mind', 'Jay-Z', 2009, 'https://www.youtube.com/watch?v=QsZlY0Vz4-o'),
(61, 'Gin N Juice', 'Snoop Dog', 1993, 'https://www.youtube.com/watch?v=DI3yXg-sX5c'),
(62, 'Godzilla', 'Eminem', 2020, 'https://www.youtube.com/watch?v=r_0JjYUe5jo'),
(63, 'Stronger', 'Kanye West', 2007, 'https://www.youtube.com/watch?v=PsO6ZnUZI0g'),
(64, 'Balance Ton Quoi ', 'Angèle', 2018, 'https://www.youtube.com/watch?v=Hi7Rx3En7-k'),
(65, 'J\'me Tire', 'Maitre Gims', 2013, 'https://www.youtube.com/watch?v=F_rEHfLgdcY'),
(66, 'Starship', 'Nicki Minaj', 2012, 'https://www.youtube.com/watch?v=SeIJmciN8mo'),
(67, 'Imagine', 'John Lennon', 1971, 'https://www.youtube.com/watch?v=VOgFZfRVaww'),
(68, 'Confessions Nocturnes', 'Diams', 2006, 'https://www.youtube.com/watch?v=TwhoaPgBwI8'),
(69, 'Stayin Alive', 'Bee Gees', 1977, 'https://www.youtube.com/watch?v=fNFzfwLM72c'),
(70, 'Memories', 'David Guetta', 2009, 'https://www.youtube.com/watch?v=NUVCQXMUVnI'),
(71, 'Let Me Love You', 'DJ Snake', 2016, 'https://www.youtube.com/watch?v=euCqAq6BRa4'),
(72, 'Animals', 'Martin Garrix', 2013, 'https://www.youtube.com/watch?v=gCYcHz2k5x0'),
(73, 'Wake Me Up', 'Avicii', 2013, 'https://www.youtube.com/watch?v=IcrbM1l_BoI'),
(74, 'Intoxicated', 'Martin Solveig', 2013, 'https://www.youtube.com/watch?v=94Rq2TX0wj4'),
(75, 'Sunset Lover', 'Petit Biscuit', 2015, 'https://www.youtube.com/watch?v=wuCK-oiE3rM'),
(76, 'I Will Survive', 'Gloria Gaynor', 1999, 'https://www.youtube.com/watch?v=ARt9HV9T0w8'),
(77, 'This Girl', 'Kungs', 2016, 'https://www.youtube.com/watch?v=2Y6Nne8RvaA'),
(78, 'Video Games', 'Lana Del Rey', 2012, 'https://www.youtube.com/watch?v=cE6wxDqdOV0'),
(79, 'What Makes You Beautiful', 'One Direction', 2011, 'https://www.youtube.com/watch?v=QJO3ROT-A4E'),
(80, 'I Gotta Feeling', 'Black Eyed Peas', 2009, 'https://www.youtube.com/watch?v=uSD4vsh1zDA');

-- --------------------------------------------------------

--
-- Table structure for table `maths-s4-series`
--

CREATE TABLE `maths-s4-series` (
  `ID` int(11) NOT NULL,
  `Titre` varchar(50) NOT NULL,
  `Lien` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `maths-s4-series`
--

INSERT INTO `maths-s4-series` (`ID`, `Titre`, `Lien`) VALUES
(1, 'Brooklyn 99', 'https://www.youtube.com/watch?v=sEOuJ4z5aTc'),
(2, 'Friends', 'https://www.youtube.com/watch?v=hDNNmeeJs1Q'),
(3, 'Breaking Bad', 'https://www.youtube.com/watch?v=HhesaQXLuRY'),
(4, 'How I Met Your Mother', 'https://www.youtube.com/watch?v=aJtVL2_fA5w'),
(5, 'The Big Bang Theory', 'https://www.youtube.com/watch?v=WBb3fojgW0Q'),
(6, 'Westworld', 'https://www.youtube.com/watch?v=9BqKiZhEFFw'),
(7, 'Game Of Thrones', 'https://www.youtube.com/watch?v=KPLWWIOCOOQ'),
(8, 'The Walking Dead', 'https://www.youtube.com/watch?v=R1v0uFms68U'),
(9, 'Stranger Things', 'https://www.youtube.com/watch?v=b9EkMc79ZSU'),
(10, 'Malcolm', 'https://www.youtube.com/watch?v=PfID_33TL_A'),
(11, 'Grace And Frankie', 'https://www.youtube.com/watch?v=CDv6PRi1SgQ'),
(12, 'MindHunter', 'https://www.youtube.com/watch?v=oFlKiTwhd38'),
(13, 'American Horror Story', 'https://www.youtube.com/watch?v=-9KZr2Vn7CQ'),
(14, 'Black Mirror', 'https://www.youtube.com/watch?v=jDiYGjp5iFg'),
(15, 'Sense 8', 'https://www.youtube.com/watch?v=iKpKAlbJ7BQ'),
(16, 'Modern Family', 'https://www.youtube.com/watch?v=aogZUDx51vQ'),
(17, 'Peaky Blinders', 'https://www.youtube.com/watch?v=oVzVdvGIC7U'),
(18, 'Orange Is The New Black', 'https://www.youtube.com/watch?v=vY0qzXi5oJg'),
(19, 'Ozark', 'https://www.youtube.com/watch?v=5hAXVqrljbs'),
(20, 'Prison Break', 'https://www.youtube.com/watch?v=AL9zLctDJaU'),
(21, 'Dr House', 'https://www.youtube.com/watch?v=MczMB8nU1sY'),
(22, 'Sherlock', 'https://www.youtube.com/watch?v=7hjPxUfV32Q'),
(23, 'The Crown', 'https://www.youtube.com/watch?v=JWtnJjn6ng0'),
(24, 'House Of Cards', 'https://www.youtube.com/watch?v=8QnMmpfKWvo'),
(25, 'How To Get Away With Murder', 'https://www.youtube.com/watch?v=dkb-aBaxkVk'),
(26, 'Suits', 'https://www.youtube.com/watch?v=85z53bAebsI'),
(27, 'Gossip Girl', 'https://www.youtube.com/watch?v=eCg1RN-dyQk'),
(28, 'Vikings', 'https://www.youtube.com/watch?v=Auzs95InJzo'),
(29, 'La Casa De Papel', 'https://www.youtube.com/watch?v=hMANIarjT50'),
(30, 'Mr Robot', 'https://www.youtube.com/watch?v=N6HGuJC--rk'),
(31, 'Sex Education', 'https://www.youtube.com/watch?v=qZhb0Vl_BaM'),
(32, 'Outlander', 'https://www.youtube.com/watch?v=PFFKjptRr7Y'),
(33, 'Grey\'s Anatomy', 'https://www.youtube.com/watch?v=q1pcpgREQ5c'),
(34, 'Desperate Housewives', 'https://www.youtube.com/watch?v=wF_VG1UGx2s'),
(35, 'Devious Maids', 'https://www.youtube.com/watch?v=K6TfckFoZAU'),
(36, 'Glee', 'https://www.youtube.com/watch?v=sefQqCMusJI'),
(37, 'Sons Of Anarchy', 'https://www.youtube.com/watch?v=Gr9_lvMuYrE'),
(38, 'The Office', 'https://www.youtube.com/watch?v=cRpbuYnHWQY'),
(39, 'Scrubs', 'https://www.youtube.com/watch?v=zOgb3KIJDY8'),
(40, 'Gotham', 'https://www.youtube.com/watch?v=9k08GueQlPg'),
(41, 'Les Demoiselles Du Téléphone', 'https://www.youtube.com/watch?v=nmOpXJRV4dQ'),
(42, 'The Handmaid\'s Tale', 'https://www.youtube.com/watch?v=dVLiDETfx1c'),
(43, 'The Witcher', 'https://www.youtube.com/watch?v=ndl1W4ltcmg'),
(44, 'Pretty Little Liars', 'https://www.youtube.com/watch?v=8_aR_3UEsrA'),
(45, '13 Reasons Why', 'https://www.youtube.com/watch?v=QkT-HIMSrRk'),
(46, 'Why Women Kill', 'https://www.youtube.com/watch?v=rFYNqmKdaoI'),
(47, 'This Is Us', 'https://www.youtube.com/watch?v=OkTEQwsE8l4'),
(48, 'The 100', 'https://www.youtube.com/watch?v=ia1Fbg96vL0'),
(49, 'Fargo', 'https://www.youtube.com/watch?v=h2tY82z3xXU'),
(50, 'The Queen\'s Gambit', 'https://www.youtube.com/watch?v=CDrieqwSdgI');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `maths-s4-series`
--
ALTER TABLE `maths-s4-series`
  ADD PRIMARY KEY (`ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
